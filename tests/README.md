# Achievements Embeddable | JS Client Support

### Description
Provides examples to consume the Achievements Embeddable Services using
JavaScript with the help of some jQuery plugins.

#### Requirements
1. jQuery
2. jQuery UI
3. jQuery Achievement Embeddable ( Proprietary )

#### Installation

1. You might need to add the following to your .httaccess file on the Server
offering the Achievement Embeddable services:

```
Header add Access-Control-Allow-Origin "*"
Header add Access-Control-Allow-Headers "origin, x-requested-with, content-type"
Header add Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
```

#### Usage
Load all of the required jQuery plugins, and then simply implement the
Achievement Embeddable plugin as shown in the index.html example file.
